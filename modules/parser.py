import argparse
from pathlib import Path

from modules.common import Type, LabelFormat


def arguments():
    root = argparse.ArgumentParser(description="Open Image Dataset v4 Downloader")

    root.add_argument('--classes', required=False, nargs='+',
                      metavar='CLASS',
                      help='space separated class names to download')

    root.add_argument('--type', required=False, type=Type,
                      choices=list(Type),
                      default=Type.all,
                      metavar=list(map(str, Type)),
                      help='set image collection type as described by the CSV file')

    root.add_argument('--csv', required=False, type=Path, default='csv/',
                      metavar='(default csv/)',
                      help='path/to/csv location folder')

    root.add_argument('--limit', required=False, type=int, default=None,
                      metavar='(default None)',
                      help='optional limit on number of images to download')

    root.add_argument('--resize', required=False, type=int, default=None,
                      metavar='(default None)',
                      help='optional resizing of images when downloading')

    root.add_argument('--threads', required=False, type=int, default=20,
                      metavar='(default 20)',
                      help='number of simultaneous threads when downloading')

    # output
    root.add_argument('--folder', required=False, type=Path, default='dataset/',
                      metavar='(default dataset/)',
                      help='path/to/dataset download folder')

    root.add_argument('--label-format', required=False, type=LabelFormat,
                      choices=list(LabelFormat),
                      default=LabelFormat.yolov3,
                      metavar=list(map(str, LabelFormat)),
                      help='output format of label files')

    root.add_argument('--label-folder', required=False, type=Path, default='.',
                      metavar='(default None)',
                      help='sub-folder to place the labels in')

    root.add_argument('--no-label', required=False, default=False, action='store_true',
                      help='disable label creation for images')

    root.add_argument('--yolo', required=False, default=False, action='store_true',
                      help='generate YOLOv3 files (data, names, list)')

    # filters
    root.add_argument('--is-occluded', required=False, type=int, choices=[0, 1], default=1,
                      metavar='[0, 1]',
                      help='indicates that the object is occluded by another object in the image')

    root.add_argument('--is-truncated', required=False, type=int, choices=[0, 1], default=1,
                      metavar='[0, 1]',
                      help='indicates that the object extends beyond the boundary of the image.')

    root.add_argument('--is-group', required=False, type=int, choices=[0, 1], default=1,
                      metavar='[0, 1]',
                      help='indicates that the box spans a group of minimum 5 objects (e.g., a bed of flowers or a '
                           'crowd of people)')

    root.add_argument('--is-depiction', required=False, type=int, choices=[0, 1], default=1,
                      metavar='[0, 1]',
                      help='indicates that the object is a depiction (e.g., a cartoon or drawing of the object, '
                           'not a real physical instance)')

    root.add_argument('--is-inside', required=False, type=int, choices=[0, 1], default=1,
                      metavar='[0, 1]',
                      help='indicates a picture taken from the inside of the object (e.g., a car interior or inside '
                           'of a building)')

    return root.parse_args()
