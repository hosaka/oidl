import math
from pathlib import Path

import requests
import pandas as pd
from pandas import DataFrame
from tqdm import tqdm

from modules.common import logger

URL = 'https://storage.googleapis.com/openimages/2018_04/'


class FileList(object):
    Class = 'class-descriptions-boxable.csv'
    Train = 'train-annotations-bbox.csv'
    Valid = 'validation-annotations-bbox.csv'
    Test = 'test-annotations-bbox.csv'


def read(file: Path, **kwargs) -> DataFrame:
    """
    Load given CSV file into the DataFrame.

    :param file: path to the CSV file
    :return: None
    """
    check(file)
    return pd.read_csv(file, **kwargs)


def check(file: Path):
    """
    Check if the CSV file is present.

    :param file: path to the CSV file
    :return: None
    """
    if not file.is_file():
        ans = input(f'Download the missing {file.name} file? [Y/n] ')

        if ans.lower() == 'y' or ans.lower() == '':
            folder = file.name.split('-')[0]

            if folder != 'class':
                save(URL + f'{folder}/', file)
            else:
                save(URL, file)


def save(url, file: Path):
    """
    Download remove CSV file printing a progress bar.

    :param url: url to download from
    :param file: file path to save the CSV
    :return: None
    """
    r = requests.get(url + file.name, stream=True)

    wrote = 0
    block = 1024
    total = int(r.headers.get('content-length', 0))

    with open(file.as_posix(), 'wb') as f:
        for data in tqdm(r.iter_content(block), total=math.ceil(total // block), unit_scale=True):
            wrote = wrote + len(data)
            f.write(data)

    if total != 0 and wrote != total:
        logger.error(f'error while downloading {file.name}')


def filter_options(df: DataFrame, args) -> DataFrame:
    """
    Filter DataFrame based on the arguments given.

    :param df: DataFrame
    :param args: argument parser
    :return: filtered DataFrame
    """
    if not args.is_occluded:
        hits = df.ImageID[df.IsOccluded != args.is_occluded].values
        df = df[~df.ImageID.isin(hits)]

    if not args.is_truncated:
        hits = df.ImageID[df.IsTruncated != args.is_truncated].values
        df = df[~df.ImageID.isin(hits)]

    if not args.is_group:
        hits = df.ImageID[df.IsGroupOf != args.is_group].values
        df = df[~df.ImageID.isin(hits)]

    if not args.is_depiction:
        hits = df.ImageID[df.IsDepiction != args.is_depiction].values
        df = df[~df.ImageID.isin(hits)]

    if not args.is_inside:
        hits = df.ImageID[df.IsInside != args.is_inside].values
        df = df[~df.ImageID.isin(hits)]

    return df
