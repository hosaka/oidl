import os
from pathlib import Path
from itertools import islice
from multiprocessing.pool import ThreadPool

from tqdm import tqdm
from pandas import DataFrame

from modules.common import logger, LabelFormat, Type
from modules.csv import filter_options


def download(args, df: DataFrame, class_name, class_code, class_index):
    """
    Download manager.

    :param args: argparse result
    :param df: pandas DataFrame with loaded CSV type
    :param class_name: class name to download
    :param class_code: class code loaded from CSV
    :param class_index: integer index of current class
    :return:
    """
    logger.info(f'downloading {args.type} images for class {class_name}')

    # filter images
    df_images = filter_options(df, args)

    images = df_images.ImageID[df_images.LabelName == class_code].values
    images = set(images)

    logger.info(f'found {len(images)} images online')

    if args.limit is not None:
        logger.info(f'limiting to {args.limit} images')
        images = set(islice(images, args.limit))

    # get images
    class_folder = args.folder.joinpath(f'{args.type}/{class_name}')
    _fetch_images(class_folder, args.type, images, args.threads)

    # resize images
    if args.resize is not None:
        _resize_images()

    # get labels
    if not args.no_label:
        label_folder = class_folder.joinpath(f'{args.label_folder}')
        _fetch_labels(df, label_folder, class_folder, class_code, class_index, args.label_format)


def _fetch_images(class_folder: Path, class_type: Type, images, threads):
    """
    Download images from Open Images Dataset v4 hosted on AWS.

    :param class_folder: class folder
    :param class_type: image subset type
    :param images: image id list loaded from CSV
    :param threads: max number of threads to use
    :return:
    """

    if not class_folder.exists():
        class_folder.mkdir(parents=True)

    # determine missing images
    downloaded = [file.stem for file in class_folder.iterdir() if file.suffix == '.jpg']
    queued = list(set(images) - set(downloaded))

    if not queued:
        logger.info(f'all images already downloaded to {class_folder}')
    else:
        logger.info(f'downloading missing {len(queued)} images to {class_folder}')

        pool = ThreadPool(threads)
        commands = [
            f'aws s3 --no-sign-request --only-show-errors cp s3://open-images-dataset/'
            f'{class_type}/{image}.jpg "{class_folder.as_posix()}"'
            for image in queued
        ]

        list(tqdm(pool.imap(os.system, commands), total=len(commands)))
        pool.close()
        pool.join()


def _resize_images():
    pass


def _fetch_labels(df: DataFrame, label_folder: Path, class_folder: Path, class_code, class_index, label_format):
    """
    Load and generate labels for downloaded images.

    :param df: pandas DataFrame
    :param label_folder: dataset folder
    :param class_code: class code loaded from CSV
    :param label_format: generated label format
    :return:
    """

    if not label_folder.exists():
        label_folder.mkdir(parents=True)

    # determine missing labels
    images = [file.stem for file in class_folder.iterdir() if file.suffix == '.jpg']
    labels = [file.stem for file in label_folder.iterdir() if file.suffix == '.txt']
    queued = list(set(images) - set(labels))

    if not queued:
        logger.info(f'all labels already present in {label_folder}')
    else:
        logger.info(f'generating missing {len(queued)} labels to {label_folder} using {label_format} format')

        groups = df[df.LabelName == class_code].groupby(df.ImageID)

        for image in tqdm(queued):

            label_file = label_folder.joinpath(f'{image}.txt')

            # TODO: would prefer a more elegant access to columns
            boxes = groups.get_group(image)[['XMin', 'XMax', 'YMin', 'YMax']].values.tolist()

            # NOTE: Ignore the variable naming for each format, they're simply named so the f.write is called
            # only in one place
            with label_file.open(mode='w') as f:
                for box in boxes:
                    # yolov3 format [center x, center y, width, height]
                    if label_format == LabelFormat.yolov3:
                        x1 = (box[0] + box[1]) / 2
                        y1 = (box[2] + box[3]) / 2
                        x2 = box[1] - box[0]
                        y2 = box[3] - box[2]

                    # voc format [left, top, right, bottom]
                    elif label_format == LabelFormat.voc:
                        for box in boxes:
                            pass

                    # original open images dataset v4 format [XMin, XMax, YMin, YMax]
                    else:
                        for box in boxes:
                            x1, y1, x2, y2 = box[0], box[1], box[2], box[3]

                    f.write(f'{class_index} {x1:.6f} {y1:.6f} {x2:.6f} {y2:.6f}\n')


def gen_yolo(args):

    dataset = args.folder

    # make file lists
    types = [f for f in dataset.iterdir() if f.is_dir() and f.stem in map(str, Type)]
    lists = {Type.train: '', Type.valid: '', Type.test: ''}

    for t in types:
        with dataset.joinpath(f'{t.stem}.list').open(mode='w') as file:
            lists[Type(t.stem)] = dataset.joinpath(f'{t.stem}.list')

            classes = [f for f in t.iterdir() if f.is_dir() and f.stem in args.classes]
            for cls in classes:
                # filter images
                images = [file for file in cls.iterdir() if file.suffix == '.jpg']

                for image in images:
                    file.write(f'{image.relative_to(dataset.parent)}\n')

    # make names
    names = dataset.joinpath(f'{dataset.stem}.names')
    with names.open(mode='w') as file:
        for cls in args.classes:
            file.write(f'{cls.lower()}\n')

    # make data
    with dataset.joinpath(f'{dataset.stem}.data').open(mode='w') as file:
        file.write(f'classes = {len(args.classes)}\n')

        file.write(f'train = {lists[Type.train]}\n')
        file.write(f'valid = {lists[Type.test]}\n')
        file.write(f'eval = {lists[Type.valid]}\n')
        file.write(f'names = {names}\n')
        file.write(f'backup = {Path("backup/")}\n')
