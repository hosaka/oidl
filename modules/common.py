import logging
from enum import Enum

APP_NAME = 'oidl'

# common logger
logger = logging.getLogger(APP_NAME)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(name)s: [%(levelname)s] %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


class StrEnum(Enum):
    def __str__(self):
        return str(self.value)


class Type(StrEnum):
    all = 'all'
    train = 'train'
    valid = 'validation'
    test = 'test'


class LabelFormat(StrEnum):
    oidv4 = 'oidv4'
    voc = 'voc'
    yolov3 = 'yolov3'
