OIDL
====
Open Images Dataset v4 Downloader

This tool is based on and inspired by https://github.com/EscVM/OIDv4_ToolKit

Props to guys over there for their hard work. I just simply adapted this for my needs and made it move YOLOv3 friendly.