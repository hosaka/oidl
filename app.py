from modules import csv

from modules import dataset
from modules.common import Type, logger, LabelFormat
from modules.parser import arguments

__author__ = 'hosaka'


if __name__ == '__main__':
    args = arguments()

    # pwd = Path(os.path.dirname(os.path.realpath(__file__)))
    class_data = csv.read(args.csv.joinpath(csv.FileList.Class), header=None)

    if args.yolo:
        #  override args in yolo mode
        args.no_label = False
        args.label_folder = None
        args.label_format = LabelFormat.yolov3

    # read all classes first and error if any not found
    class_codes = []
    for class_name in args.classes:
        class_row = class_data.loc[class_data[1] == class_name]

        if class_row.empty:
            logger.error(f'class {class_name} is not present in {args.csv.joinpath(csv.FileList.Class)} file')
            exit()
        else:
            # cache class codes
            class_codes.append(class_row.values.item(0))

    for idx, class_name in enumerate(args.classes):
        if args.type == Type.train:
            csv_path = args.csv.joinpath(csv.FileList.Train)

        elif args.type == Type.valid:
            csv_path = args.csv.joinpath(csv.FileList.Valid)

        elif args.type == Type.test:
            csv_path = args.csv.joinpath(csv.FileList.Test)

        # args.type == Type.all
        else:
            break

        # load pandas object
        data = csv.read(csv_path)

        # download images
        dataset.download(args, data, class_name, class_codes[idx], idx)

    if args.yolo:
        dataset.gen_yolo(args)

    logger.info('all done')
